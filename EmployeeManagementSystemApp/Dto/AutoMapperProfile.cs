﻿using System;
using AutoMapper;
using EmployeeManagementSystemApp.Models;

namespace EmployeeManagementSystemApp.Dto
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<RegisterModel, RegisterModelDto>();
        }
    }
}
