﻿using System;
using System.ComponentModel.DataAnnotations;
using EmployeeManagementSystemApp.Models;

namespace EmployeeManagementSystemApp.Dto
{
    public class RegisterModelDto
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "This First Name Field is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "This Last Name Field is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "This Email Field is required")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "This Password Field is required")]
        public string Password { get; set; }

        public Role Role { get; set; }

        [Required(ErrorMessage = "This Department Field is required")]
        public string Department { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-ddThh:mm:ss}")]
        [Display(Name = "Date Of Birth")]
        public DateTime DateOfBirth { get; set; }
    }
}
