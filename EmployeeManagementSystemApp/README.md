﻿# Very short description of this App

This is just a simple Employee Management Application with asp.net core3.1 mvc

## Dependencies

## MYSQL: It is expected that you have a mysql database you can either create new database connection call it "heirsholding".

you might want to check the appsettings.json for the right connection strings of your system in :

    "DefaultConnection": "Server=localhost, 3306; Database=heirsholding; User=root; Password=;"

and edit to match the configuration of your database

## Visual studio: Start the app with visual studio 

## dotnet ef

install with:

```bash
dotnet tool install --global dotnet-ef --version 3.0.0
```

An already generated migration file exist by running:

```bash
dotnet ef database update
```
this command generates table and seeds the admin user into the database "heirsholding"

Kindly ensure you create a connection or update the connection in the appsettings.json file before running this code.
 
a dump was also sent so it might not be necessary.


## Getting Started
    There are two methods for getting started with this repo. 


    1. Download the .zip file. Extract the contents of the zip file then change directory to the file

    2. Checkout this repo, install dependencies, then start the gulp process with the following:

```bash
    > git clone https://ayocodes@bitbucket.org/ayocodes/employee-management-system.git
    > cd EmployeeManagementSystemApp
```


## Features
- Admin User can login
- Admin User can create employees
- Admin user can view all the list of employees added
- Admin user can filter employee details ( first name , last name , date of birth  , department).


## Migration:

## RUN 

To generate a migration use:
```bash
    dotnet ef migrations add InitialCreate
```

To run a migration use:
```bash
dotnet ef database update
```