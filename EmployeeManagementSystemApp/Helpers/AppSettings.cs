﻿using System;
namespace EmployeeManagementSystemApp.Helpers
{
    public class AppSettings
    {
        //Jwt Properties
        public string Site { get; set; }
        public string Audience { get; set; }
        public string ExpireTime { get; set; }
        public string Secret { get; set; }
    }
}
