﻿using System;
using System.Security.Cryptography;
using System.Text;
using EmployeeManagementSystemApp.Models;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagementSystemApp.Helpers
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<RegisterModel> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<RegisterModel>(entity => {
                entity.HasIndex(e => e.Email).IsUnique();
                entity.Property(e => e.Role)
            .HasMaxLength(50)
            .HasConversion(x => x.ToString(), // to converter
                x => (Role)Enum.Parse(typeof(Role), x));
                ;
            });

            builder.Entity<RegisterModel>()
        .HasData(
            new RegisterModel
            {
                Id =1,
                FirstName = "Admin",
                LastName = "Admin",
                Email = "admin@admin.com",
                Password = GetMD5("admin"),
                Role = Role.admin,
                Department = "Tech",
                DateOfBirth = new DateTime(1990, 12, 25, 10, 30, 50)
            }
        );
        }

        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }
    }
}