﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using EmployeeManagementSystemApp.Helpers;
using EmployeeManagementSystemApp.Models;
using EmployeeManagementSystemApp.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace EmployeeManagementSystemApp.Controllers
{

    public class LoginController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IUserRefreshTokenRepository _userRefreshTokenRepository;
        private ApplicationDbContext _dbContext;
        private readonly AppSettings _appsettings;

        public LoginController(IConfiguration configuration, IUserRefreshTokenRepository userRefreshTokenRepository, IOptions<AppSettings> appSettings, IOptions<Config> connection, ApplicationDbContext dbContext)
        {
            _configuration = configuration;
            _userRefreshTokenRepository = userRefreshTokenRepository;
            _dbContext = dbContext;

        }

        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [Route("login")]
        [HttpPost]
        public IActionResult Login([FromForm] LoginModel user)
        {

            if (string.IsNullOrEmpty(user.Email) || string.IsNullOrEmpty(user.Password))
            {
                return (RedirectToAction("Error"));
            }

            IActionResult response = Unauthorized();
            if (ModelState.IsValid)
            {
                var f_password = GetMD5(user.Password);

                var data = _dbContext.Users.Where(s => s.Email.Equals(user.Email) && s.Password.Equals(f_password)).ToList();
                if (data.Count() > 0)
                {
                    var jwtToken = GenerateJwtToken(user);

                    _userRefreshTokenRepository.SaveOrUpdateUserRefreshToken(new UserRefreshToken
                    {
                        RefreshToken = jwtToken.RefreshToken,
                        Username = user.Email
                    });

                    if (jwtToken != null)
                    {
                        HttpContext.Session.SetString("Token", jwtToken.Token);

                        return RedirectToAction("Index", "Users", new { area = "" });
                    }
                    else
                    {
                        return (RedirectToAction("Error"));
                    }
                }
                else
                {

                    return View(user);
                }

            }

            return BadRequest("Invalid user");
        }

        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }

        [HttpPost]
        [Route("refreshToken")]
        public IActionResult RefreshToken([FromBody] JwtToken jwtToken)
        {
            if (jwtToken == null)
            {
                return BadRequest("Invalid request");
            }

            var handler = new JwtSecurityTokenHandler();

            SecurityToken validatedToken;

            IPrincipal principal = handler.ValidateToken(jwtToken.Token, GetTokenValidationParameters(), out validatedToken);

            var username = principal.Identity.Name;

            if (_userRefreshTokenRepository.CheckIfRefreshTokenIsValid(username, jwtToken.RefreshToken))
            {
                var user = new LoginModel { Email = username };
                var newJwtToken = GenerateJwtToken(user);

                _userRefreshTokenRepository.SaveOrUpdateUserRefreshToken(new UserRefreshToken
                {
                    Username = user.Email,
                    RefreshToken = newJwtToken.RefreshToken
                });

                return Ok(newJwtToken);
            }

            return BadRequest("Invalid Request");

        }

        private TokenValidationParameters GetTokenValidationParameters()
        {
            var securityKey = Encoding.UTF8.GetBytes(_configuration["Jwt:Secret"]);

            return new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(securityKey),
                ValidIssuers = new string[] { _configuration["Jwt:Issuer"] },
                ValidAudiences = new string[] { _configuration["Jwt:Issuer"] },
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true
            };
        }

        private JwtToken GenerateJwtToken(LoginModel user)
        {
            var securityKey = Encoding.UTF8.GetBytes(_configuration["Jwt:Secret"]);

            var claims = new Claim[] {
                    new Claim(ClaimTypes.Name,user.Email),
                    new Claim(ClaimTypes.Email,user.Email)
                };

            var credentials = new SigningCredentials(new SymmetricSecurityKey(securityKey), SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
              _configuration["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddDays(7),
              signingCredentials: credentials);


            var jwtToken = new JwtToken
            {
                RefreshToken = new RefreshTokenGenerator().GenerateRefreshToken(32),
                Token = new JwtSecurityTokenHandler().WriteToken(token)
            };

            return jwtToken;
        }

        public IActionResult Error()
        {
            ViewBag.Message = "An error occured...";
            return View();
        }
    }


}
