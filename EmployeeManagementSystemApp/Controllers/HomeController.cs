﻿using EmployeeManagementSystemApp.Helpers;
using EmployeeManagementSystemApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Linq.Dynamic.Core;
using EmployeeManagementSystemApp.Dto;

namespace EmployeeManagementSystemApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _config;
        private string generatedToken = null;
        private readonly AppSettings _appsettings;
        private ApplicationDbContext _dbContext;

        public HomeController(IConfiguration config, IOptions<AppSettings> appSettings, IOptions<Config> connection, ApplicationDbContext dbContext)
        {
            _config = config;
            _appsettings = appSettings.Value;
            _dbContext = dbContext;
        }

        public IActionResult Index()
        {
            return View();
            //return RedirectToAction("Login", "Users", new { area = "" });

        }

        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }

        [Authorize]
        [Route("dashboard")]
        [HttpGet]
        public IActionResult Dashboard()
        {
            string token = HttpContext.Session.GetString("Token");

            if (token == null)
            {
                return (RedirectToAction("Login", "index"));
            }

            return View();
        }

        [ValidateAntiForgeryToken]
        [Route("register")]
        [HttpPost]
        public IActionResult Register([FromForm]RegisterModelDto model)
        {
            List<string> errorList = new List<string>();

            if (ModelState.IsValid)
            {
                var password = GetMD5(model.Password);

                var NewUser = new RegisterModel
                {
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    DateOfBirth = model.DateOfBirth,
                    Department = model.Department,
                    Role = model.Role,
                    Password = password
                };

                _dbContext.Users.Add(NewUser);

                _dbContext.SaveChanges();


                return Json(new { success = true, message = "Saved successfully" });
            }
            else
            {

                return View(model);
            }
        }

        //Logout
        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }

        [HttpGet]
        [Route("register")]
        public IActionResult Register()
        {

            return View();
        }

        public IActionResult Error()
        {
            ViewBag.Message = "An error occured...";
            return View();
        }

        [Route("users")]
        [HttpGet]
        public ActionResult Users()
        {
            string token = HttpContext.Session.GetString("Token");

            if (token == null)
            {
                return (RedirectToAction("Login", "login"));
            }

            return View();
        }

        public ActionResult EmployeeData()
        {
            var users = _dbContext.Users.ToList();


            return Json(new { data = users });
        }


        private string BuildMessage(string stringToSplit, int chunkSize)
        {
            var data = Enumerable.Range(0, stringToSplit.Length / chunkSize)
                .Select(i => stringToSplit.Substring(i * chunkSize, chunkSize));

            string result = "The generated token is:";

            foreach (string str in data)
            {
                result += Environment.NewLine + str;
            }

            return result;
        }
    }
}

