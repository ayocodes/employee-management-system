﻿using System;
namespace EmployeeManagementSystemApp.Models
{
    public class UserRefreshToken
    {
        public string Username { get; set; }
        public string RefreshToken { get; set; }
    }
}
