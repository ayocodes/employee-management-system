﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmployeeManagementSystemApp.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Your Email is Required.")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Your Password is Required.")]
        public string Password { get; set; }
    }
}
